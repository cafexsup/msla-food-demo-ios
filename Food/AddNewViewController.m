//
//  AddNewViewController.m
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "AddNewViewController.h"

@interface AddNewViewController ()
@property (nonatomic, assign) BOOL shownLogin;
@property (nonatomic, weak) IBOutlet UIImageView *tabViewBg;
@property (nonatomic, weak) IBOutlet UIView *view1;

@end

@implementation AddNewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
                // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.tabBarController.tabBar.tintColor = [UIColor blackColor];
    for (int index=0; index<self.tabBarController.tabBar.items.count; index++) {
        UITabBarItem *item = [self.tabBarController.tabBar.items objectAtIndex:index];
        if (index == 0) {
            item.image = [[UIImage imageNamed:@"icon-tab-add-new.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            item.selectedImage = [[UIImage imageNamed:@"icon-tab-add-new-selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        if (index == 1) {
            item.image = [[UIImage imageNamed:@"icon-tab-recipes.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            item.selectedImage = [[UIImage imageNamed:@"icon-tab-recipes-selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        if (index == 2) {
            item.image = [[UIImage imageNamed:@"icon-tab-wine.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            item.selectedImage = [[UIImage imageNamed:@"icon-tab-wine-selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        if (index == 3) {
            item.image = [[UIImage imageNamed:@"icon-tab-profile.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            item.selectedImage = [[UIImage imageNamed:@"icon-tab-profile-selected.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
    }
    
    self.carousel.type = iCarouselTypeRotary;
    [self.carousel scrollToItemAtIndex:5 animated:NO];
    
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    if (!self.shownLogin) {
        [self performSegueWithIdentifier:@"showLogin" sender:nil];
        self.shownLogin = YES;
    }
    [super viewWillAppear:animated];
}

-(IBAction)tabSelected:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
            self.tabViewBg.image = [UIImage imageNamed:@"tab1_selected"];
          // self.view1.hidden = NO;
            break;
        case 2:
            self.tabViewBg.image = [UIImage imageNamed:@"tab1_selected"];
          //  self.view1.hidden = YES;
            break;
        case 3:
            self.tabViewBg.image = [UIImage imageNamed:@"tab1_selected"];
          //  self.view1.hidden = YES;
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return 5;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    //create a numbered view
    view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 543, 407)];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 543, 407)];
    img.image = [UIImage imageNamed:@"stadium-slider-bg.png"];
    [view addSubview:img];
    
    UIImageView *imgStadium = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 543, 407)];
    if (index%3==0)
        imgStadium.image = [UIImage imageNamed:@"slide-img1.png"];
    if (index%3==1)
        imgStadium.image = [UIImage imageNamed:@"slide-img2.png"];
    if (index%3==2)
        imgStadium.image = [UIImage imageNamed:@"slide-img3.png"];
    view.clipsToBounds = YES;
    
    [view addSubview:imgStadium];
    return view;
}


-(IBAction)liveBtnTapped:(id)sender
{
    [self performSegueWithIdentifier:@"PushToLive" sender:sender];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        default:
        {
            return value;
        }
    }
}

-(void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    [self performSegueWithIdentifier:@"PushToLive" sender:nil];
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel
{
	NSLog(@"Carousel will begin dragging");
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate
{
	NSLog(@"Carousel did end dragging and %@ decelerate", decelerate? @"will": @"won't");
}

- (void)carouselWillBeginDecelerating:(iCarousel *)carousel
{
	NSLog(@"Carousel will begin decelerating");
}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel
{
	NSLog(@"Carousel did end decelerating");
}

- (void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel
{
	NSLog(@"Carousel will begin scrolling");
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
	NSLog(@"Carousel did end scrolling");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
