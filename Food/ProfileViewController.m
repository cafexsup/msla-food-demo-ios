//
//  ProfileViewController.m
//  Food
//
//  Created by My App Templates Team on 16/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@property (nonatomic, weak) IBOutlet UIImageView *imgView1;
@property (nonatomic, weak) IBOutlet UIImageView *imgView2;
@property (nonatomic, weak) IBOutlet UIImageView *imgView3;

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createRotationForView:self.imgView1 duration:0.5];
    [self createRotationForView:self.imgView2 duration:0.5];
    [self createRotationForView:self.imgView3 duration:0.5];
    // Do any additional setup after loading the view.
}

- (void) createRotationForView:(UIView *)view duration:(double) duration {
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat:M_PI * 2.0 * 1 * 1];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = 1;
    rotationAnimation.repeatCount = 1000;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
