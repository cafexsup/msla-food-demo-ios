
//
//  LoginViewController.m
//  Food
//
//  Created by My App Templates Team on 16/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController () {

}
@property (nonatomic, weak) IBOutlet UIScrollView *scrollContent;
@property (nonatomic, weak) IBOutlet UITextField *txtUsername;
@property (nonatomic, weak) IBOutlet UITextField *txtPassword;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)keyboardWillShow:(NSNotification*)aNotification {
    [self.scrollContent setContentOffset:CGPointMake(0, 100) animated:YES];
    
}

- (void)keyboardWillHide:(NSNotification*)aNotification {
    [self.scrollContent setContentOffset:CGPointMake(0, 0) animated:YES];
    
}

- (IBAction)loginBtnTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) rememberMe:(UIButton *)sender {
    sender.selected = !sender.selected;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.txtUsername) {
        [self.txtPassword becomeFirstResponder];
    } else {
        [self.txtPassword resignFirstResponder];
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
