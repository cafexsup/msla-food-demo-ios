//
//  AddNewViewController.h
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iCarousel.h"

@interface AddNewViewController : ParentViewController <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) IBOutlet iCarousel *carousel;

@end
