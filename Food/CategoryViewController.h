//
//  CategoryViewController.h
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "ParentViewController.h"

@interface CategoryViewController : ParentViewController <UITableViewDataSource, UITableViewDelegate>

@end
