//
//  RecipeViewController.m
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "RecipeViewController.h"
#import "RecipeTableViewCell.h"

@interface RecipeViewController () {
    NSMutableArray *arrOfRecipes;
}
@property (nonatomic, weak) IBOutlet UITableView *tblRecipes;

@end

@implementation RecipeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    arrOfRecipes = [[NSMutableArray alloc]init];
    [self populateRecipesArray];
    self.title = self.category;
    self.tblRecipes.layer.borderColor = [UIColor colorWithRed:212.0/255.0 green:162.0/255.0 blue:112.0/255.0 alpha:1].CGColor;
    self.tblRecipes.layer.borderWidth =1.0f;
    self.tblRecipes.layer.cornerRadius = 2.0f;
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)populateRecipesArray{
    for (int i=0; i<3; i++) {
        [self addIconImage:@"dessert-left-img1.png" name:@"Mandarin Sorbet" author:@"by John Doe"];
        [self addIconImage:@"dessert-left-img2.png" name:@"Blackberry Sorbet" author:@"by Stanislav Kirilov"];
        [self addIconImage:@"dessert-left-img3.png" name:@"Mango Sorbet" author:@"by Scarlet Johanson"];
        [self addIconImage:@"dessert-left-img4.png" name:@"Mandarin Sorbet" author:@"by John Doe"];
        [self addIconImage:@"dessert-left-img5.png" name:@"Blackberry Sorbet" author:@"by Stanislav Kirilov"];
        [self addIconImage:@"dessert-left-img6.png" name:@"Mango Sorbet" author:@"by Scarlet Johanson"];
    }
}

- (void)addIconImage:(NSString *)iconFile name:(NSString *)name author:(NSString *)author {
    [arrOfRecipes addObject:@{@"iconFile":iconFile, @"name":name, @"author":author}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView Datasource/Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrOfRecipes.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecipeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReceipeCell"];
    NSDictionary *recipeDic = [arrOfRecipes objectAtIndex:indexPath.row];
    cell.imgRecipeIcon.image = [UIImage imageNamed:recipeDic[@"iconFile"]];
    cell.lblName.text = recipeDic[@"name"];
    cell.lblAuthor.text = recipeDic[@"author"];
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
