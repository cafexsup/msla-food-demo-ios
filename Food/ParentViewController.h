//
//  ParentViewController.h
//
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentViewController : UIViewController {
    UIPopoverController *popOverController;
}

-(IBAction)searchBtnTapped:(id)sender;
@end
