//
//  SearchViewController.m
//  Food
//
//  Created by My App Templates Team on 16/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()
{
    NSDictionary *itemDic;
    NSArray *sectionTitle;
}
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.preferredContentSize = CGSizeMake(320, 568);
    itemDic = @{@"A" : @[@"Item", @"Item", @"Item"],
                @"C" : @[@"Item", @"Item", @"Item"],
                @"C" : @[@"Item", @"Item", @"Item"],
                @"D" : @[@"Item", @"Item", @"Item"],
                @"E" : @[@"Item", @"Item", @"Item"],
                @"G" : @[@"Item", @"Item", @"Item"],
                @"H" : @[@"Item", @"Item", @"Item"],
                @"K" : @[@"Item", @"Item", @"Item"],
                @"L" : @[@"Item", @"Item", @"Item"],
                @"M" : @[@"Item", @"Item", @"Item"],
                @"P" : @[@"Item", @"Item", @"Item"],
                @"R" : @[@"Item", @"Item", @"Item"],
                @"S" : @[@"Item", @"Item", @"Item"],
                @"T" : @[@"Item", @"Item", @"Item"],
                @"W" : @[@"Item", @"Item", @"Item"]};
    
    sectionTitle = [[itemDic allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [sectionTitle count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [sectionTitle objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitles = [sectionTitle objectAtIndex:section];
    NSArray *sectionItems = [itemDic objectForKey:sectionTitles];
    return [sectionItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Configure the cell...
    NSString *sectionTitles = [sectionTitle objectAtIndex:indexPath.section];
    NSArray *sectionItems = [itemDic objectForKey:sectionTitles];
    NSString *animal = [sectionItems objectAtIndex:indexPath.row];
    cell.textLabel.text = animal;
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return sectionTitle;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
