//
//  RecipeViewController.h
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "ParentViewController.h"

@interface RecipeViewController : ParentViewController

@property (nonatomic, retain) NSString *category;
@end
