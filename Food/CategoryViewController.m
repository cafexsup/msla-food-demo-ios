//
//  CategoryViewController.m
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "CategoryViewController.h"
#import "RecipeViewController.h"
#import "CustomButton.h"

#define INNER_VIEW_WIDTH 260
#define INNER_VIEW_HEIGHT 140

@interface CategoryViewController () {
    NSMutableDictionary *dicOfViews;
    NSMutableArray *arrOfCategories;
}
@property (nonatomic, weak) IBOutlet UITableView *tblCategories;

@end

@implementation CategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    dicOfViews = [[NSMutableDictionary alloc]init];
    arrOfCategories = [[NSMutableArray alloc]init];
    [self createSourceData];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (void)createSourceData{
    for (int i=0; i<5; i++) {
        [arrOfCategories addObject:[[NSMutableArray alloc]init]];
    }
    
    [self addIconImage:@"appetiser-pic-1.png" name:@"Mandarin Sorbet" category:@"Appetizers" atIndex:0];
    [self addIconImage:@"appetiser-pic-2.png" name:@"Brown Rice Sushi" category:@"Appetizers" atIndex:0];
    [self addIconImage:@"appetiser-pic-3.png" name:@"Mini BLTs" category:@"Appetizers" atIndex:0];
    [self addIconImage:@"appetiser-pic-1.png" name:@"Mandarin Sorbet" category:@"Appetizers" atIndex:0];
    
    [self addIconImage:@"beverages-pic-1.png" name:@"Maitai" category:@"Beverages" atIndex:1];
    [self addIconImage:@"beverages-pic-2.png" name:@"Vodka Martini" category:@"Beverages" atIndex:1];
    [self addIconImage:@"beverages-pic-3.png" name:@"Mini BLTs" category:@"Beverages" atIndex:1];
    [self addIconImage:@"beverages-pic-1.png" name:@"Maitai" category:@"Beverages" atIndex:1];
    
    [self addIconImage:@"breakfast-pic-1.png" name:@"The Full English" category:@"Breakfasts" atIndex:2];
    [self addIconImage:@"breakfast-pic-2.png" name:@"Vegetarian Mushroom Omlet" category:@"Breakfasts" atIndex:2];
    [self addIconImage:@"breakfast-pic-3.png" name:@"Italian" category:@"Breakfasts" atIndex:2];
    [self addIconImage:@"breakfast-pic-1.png" name:@"The Full English" category:@"Breakfasts" atIndex:2];
    
    [self addIconImage:@"main-dishes-pic1.png" name:@"Paella Valenciana" category:@"Main Dishes" atIndex:3];
    [self addIconImage:@"main-dishes-pic2.png" name:@"Pasta Bolognese sauce" category:@"Main Dishes" atIndex:3];
    [self addIconImage:@"main-dishes-pic3.png" name:@"American Burger" category:@"Main Dishes" atIndex:3];
    [self addIconImage:@"main-dishes-pic1.png" name:@"Paella Valenciana" category:@"Main Dishes" atIndex:3];
    
    [self addIconImage:@"appetiser-pic-1.png" name:@"Mandarin Sorbet" category:@"Typical Cakes" atIndex:4];
    [self addIconImage:@"appetiser-pic-2.png" name:@"Brown Rice Sushi" category:@"Typical Cakes" atIndex:4];
    [self addIconImage:@"appetiser-pic-3.png" name:@"Mini BLTs" category:@"Typical Cakes" atIndex:4];
    [self addIconImage:@"appetiser-pic-1.png" name:@"Mandarin Sorbet" category:@"Typical Cakes" atIndex:4];
    
}

-(void)addIconImage:(NSString *)iconFile name:(NSString *)name category:(NSString *)cat atIndex:(int)index {
    [[arrOfCategories objectAtIndex:index] addObject:@{@"iconFile":iconFile, @"name":name, @"cat":cat}];
}

-(NSMutableDictionary *)emptyDictionary {
    return [[NSMutableDictionary alloc]init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return arrOfCategories.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    for (UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIScrollView class]]){
            [view removeFromSuperview];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView addSubview:[self getScrollViewForCell:indexPath cell:cell]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 768, 50)];
    NSMutableDictionary *dicOfImage = [[arrOfCategories objectAtIndex:section] objectAtIndex:0];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(24, 0, 200, 50)];
    lblTitle.text = [dicOfImage objectForKey:@"cat"];
    lblTitle.font = [UIFont systemFontOfSize:20];
    
    UILabel *lblViewAll = [[UILabel alloc] initWithFrame:CGRectMake(550, 0, 180, 50)];
    lblViewAll.text = [NSString stringWithFormat:@"All %@",[dicOfImage objectForKey:@"cat"]];
    lblViewAll.font = [UIFont systemFontOfSize:20];
    lblViewAll.textAlignment = NSTextAlignmentRight;
    
    UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(720, 0, 50, 50)];
    img.image = [UIImage imageNamed:@"arrow-next-black.png"];
    img.contentMode = UIViewContentModeCenter;
    
    CustomButton *customBtn = [[CustomButton alloc]initWithFrame:CGRectMake(0, 0, 768, 50)];
    [customBtn addTarget:self action:@selector(customBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:lblTitle];
    [view addSubview:lblViewAll];
    [view addSubview:customBtn];
    [view addSubview:img];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UIScrollView *)getScrollViewForCell:(NSIndexPath *)indexPath cell:(UITableViewCell *)cell{
    if (![dicOfViews objectForKey:indexPath]) {
        UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(10, 0, cell.contentView.frame.size.width-10, INNER_VIEW_HEIGHT)];
        scroll.backgroundColor = [UIColor grayColor];
        for (int i=0; i<[[arrOfCategories objectAtIndex:indexPath.section] count]; i++) {
            NSMutableDictionary *dicOfImage = [[arrOfCategories objectAtIndex:indexPath.section] objectAtIndex:i];
            UIView *view = [[UIView alloc]initWithFrame:CGRectMake(1+i*INNER_VIEW_WIDTH, 0, INNER_VIEW_WIDTH-1, INNER_VIEW_HEIGHT)];
            UIImageView *bgImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 1, INNER_VIEW_WIDTH-1, INNER_VIEW_HEIGHT-1)];
            bgImg.image = [UIImage imageNamed:[dicOfImage objectForKey:@"iconFile"]];
            UIImageView *stripImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 98, INNER_VIEW_WIDTH-1, 42)];
            stripImg.image = [UIImage imageNamed:@"category-thumbnail-transparent-bg.png"];
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 94, INNER_VIEW_WIDTH-11, 44)];
            lblTitle.text = [dicOfImage objectForKey:@"name"];
            lblTitle.textColor = [UIColor whiteColor];
            lblTitle.font = [UIFont systemFontOfSize:20];
            [view addSubview:bgImg];
            [view addSubview:stripImg];
            [view addSubview:lblTitle];
            [scroll addSubview:view];
            [scroll setContentSize:CGSizeMake(view.frame.origin.x+INNER_VIEW_WIDTH,scroll.frame.size.height)];
        }
        [dicOfViews setObject:scroll forKey:indexPath];
    }
    return [dicOfViews objectForKey:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"pushToRecipie" sender:indexPath];
}

-(IBAction)customBtnTapped:(CustomButton *)sender {
    [self performSegueWithIdentifier:@"pushToRecipie" sender:sender.indexPath];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    RecipeViewController *recipeVC = segue.destinationViewController;
    NSIndexPath *indexPath = (NSIndexPath *)sender;
    recipeVC.category = [[[arrOfCategories objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"cat"];
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
