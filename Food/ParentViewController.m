//
//  ParentViewController.m
//  iMate
//
//  Created by Jimmy on 23/05/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "ParentViewController.h"
#import "SearchViewController.h"
#import <liveassistfor365/liveassistfor365.h>

@interface ParentViewController ()

@end

@implementation ParentViewController


- (void)registerDefaultsFromSettingsBundle {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle) {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key && [[prefSpecification allKeys] containsObject:@"DefaultValue"]) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    //[defaultsToRegister release];
    
    NSDictionary *std = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"standardUserDefaults: %@", std);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
     self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor]};
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [self registerDefaultsFromSettingsBundle];
    
    NSString *siteId = [[NSUserDefaults standardUserDefaults] stringForKey:@"site_id"];
    NSString *section = [[NSUserDefaults standardUserDefaults] stringForKey:@"section"];
    
    NSLog(@"*** siteId: %@", siteId);
    NSLog(@"*** section: %@", section);
    
    int accountId = siteId.intValue;
    Boolean fullScreen = NO;
    NSString *sections = section;
    AssistConfig *assistConfig = [AssistConfig withAccountId:accountId sections:sections fullScreen:fullScreen frame:self.view.frame];
    
    NSLog(@"Assist Config: %@",assistConfig);
    
    LiveAssistView *assistView = [[LiveAssistView alloc] initWithAssistConfig:assistConfig];
    
    [self.view addSubview:assistView];

}

-(IBAction)searchBtnTapped:(id)sender {
    SearchViewController *searchVC = [[SearchViewController alloc]initWithNibName:@"SearchViewController" bundle:nil];
    popOverController = [[UIPopoverController alloc] initWithContentViewController:searchVC];
    [popOverController presentPopoverFromBarButtonItem:(UIBarButtonItem *)sender
                                permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
