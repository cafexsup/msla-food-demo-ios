//
//  ViewController.m
//  Food
//
//  Created by My App Templates Team on 15/09/14.
//  Copyright (c) 2014 MyAppTemplates. All rights reserved.
//

#import "ViewController.h"
#import <liveassistfor365/liveassistfor365.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)registerDefaultsFromSettingsBundle {
    NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
    if(!settingsBundle) {
        NSLog(@"Could not find Settings.bundle");
        return;
    }
    
    NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
    NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
    
    NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
    for(NSDictionary *prefSpecification in preferences) {
        NSString *key = [prefSpecification objectForKey:@"Key"];
        if(key && [[prefSpecification allKeys] containsObject:@"DefaultValue"]) {
            [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    //[defaultsToRegister release];
    
    NSDictionary *std = [NSUserDefaults standardUserDefaults];
    
    NSLog(@"standardUserDefaults: %@", std);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Do any additional setup after loading the view.
    
    [self registerDefaultsFromSettingsBundle];
    
    NSString *siteId = [[NSUserDefaults standardUserDefaults] stringForKey:@"site_id"];
    NSString *section = [[NSUserDefaults standardUserDefaults] stringForKey:@"section"];
    
    NSLog(@"*** siteId: %@", siteId);
    NSLog(@"*** section: %@", section);
    
    int accountId = siteId.intValue;
    Boolean fullScreen = NO;
    NSString *sections = section;
    AssistConfig *assistConfig = [AssistConfig withAccountId:accountId sections:sections fullScreen:fullScreen frame:self.view.frame];
    
    NSLog(@"Assist Config: %@",assistConfig);
    
    LiveAssistView *assistView = [[LiveAssistView alloc] initWithAssistConfig:assistConfig];
    
    [self.view addSubview:assistView];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
