#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AssistConfig : NSObject

@property NSString *url;
@property BOOL fullScreen;
@property NSInteger accountId;
@property CGRect frame;
@property NSString* sections;

+(instancetype) withAccountId : (NSInteger) accountId url : (NSString*) url fullScreen : (BOOL) fullScreen frame : (CGRect) frame;

+(instancetype) withAccountId : (NSInteger) accountId sections : (NSString*) sections fullScreen : (BOOL) fullScreen frame : (CGRect) frame;



@end
