#import <UIKit/UIKit.h>
#import "AssistConfig.h"

@interface LiveAssistView : UIWebView<UIWebViewDelegate>

- (instancetype)initWithAssistConfig : (AssistConfig*) assistConfig;
-(void) setSection : (NSString*) section;
-(void) setBrowserMode : (BOOL) browserMode;

@end
