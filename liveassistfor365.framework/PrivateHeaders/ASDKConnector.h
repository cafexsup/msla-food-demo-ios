#import <Foundation/Foundation.h>

@interface ASDKConnector : NSObject
- (void) reconnect:(float) maxTimeout;
- (void) terminate:(NSError *) reason;
@end
