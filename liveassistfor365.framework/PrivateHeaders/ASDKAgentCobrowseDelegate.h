#import "ASDKAgent.h"

@protocol ASDKAgentCobrowseDelegate<NSObject>
@required

/*!
 * Called when an Agent has joined the root topic irrespective of whether they join
 * the cobrowse sub-topic.
 * @param agent the joining agent.
 */
- (void) agentJoinedSession:(ASDKAgent *) agent;

/*!
 * Called when an Agent disconnects from the root topic
 * @param agent the leaving agent.
 */
- (void) agentLeftSession:(ASDKAgent *)agent;

/*!
 * Called when an Agent has requested cobrowsing with the consumer.
 * This is only called once when the Agent initially requests to cobrowse.
 * @param agent the requesting agent.
 */
- (void) agentRequestedCobrowse:(ASDKAgent *) agent;

/*!
 * Called when an Agent joins the cobrowse sub-topic.
 * The Agent will only be able to join the cobrowse sub-topic once the consumer
 * has allowed/permitted it.
 * @param agent the joining agent.
 */
- (void) agentJoinedCobrowse:(ASDKAgent *) agent;

/*!
 * Called when an Agent leaves the cobrowse sub-topic.
 * The Agent will leave a cobrowse sub-topic in the following scenarios:
 *  The Agent disconnects their WebSocket;
 *  The Consumer disallows the Agent.
 * @param agent the leaving agent.
 */
- (void) agentLeftCobrowse:(ASDKAgent *)agent;

@end