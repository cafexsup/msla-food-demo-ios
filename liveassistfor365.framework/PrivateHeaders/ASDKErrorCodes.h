
typedef enum ASDKErrorCode : int {
    ASDKERRCalleeNotFound = 20101,
    ASDKERRCalleeBusy,
    ASDKERRCallCreationFailed,
    ASDKERRCallTimeout,
    ASDKERRCallFailed,
    ASDKERRCSDKSessionFailure,
    ASDKERRCameraNotAuthorized,
    ASDKERRMicrophoneNotAuthorized,
    
    ASDKERRAssistSessionCreationFailure = 30101,
    ASDKERRAssistTransportFailure,
    ASDKERRAssistSessionInProgress,
    
    ASDKERRAssistConsumerDocumentShareFailedNotScreenSharing = 40101,
    
    ASDKAssistSupportEnded = 50101
    
} ASDKErrorCode;
