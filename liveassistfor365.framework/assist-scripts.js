var elementToAppearWaitTimeInMS = 500;

function addMutationObserver() {
    
    var observer = new MutationObserver(function (mutations) {
                                        
                                        for (var i = 0; i < mutations.length; ++i) {
                                        for (var j = 0; j < mutations[i].addedNodes.length; ++j) {
                                        var id = mutations[i].addedNodes[j].id;
                                        console.log(id);
                                        if (id.includes('LPMcontainer') == true)
                                        getLivePersonRectangles();
                                        sendMessage("TAG IS ON THE PAGE..", "tagActive");
                                        }
                                        }
                                        
                                        removePopoutButton();
                                        
                                        window.setTimeout(function () {
                                                          getLivePersonRectangles();
                                                          }, elementToAppearWaitTimeInMS);
                                        
                                        
                                        });
    var observerConfig = {
    childList: true,
    subtree: true,
    attributes: true,
    characterData: false
    };
    
    var targetNode = document.body;
    observer.observe(targetNode, observerConfig);
    
}

function removePopoutButton() {
    var popout = document.getElementsByClassName('lp_popout');
    for (var j = 0; j != popout.length; j++) {
        popout[j].style.display = 'none';
    }
}

function addLivePerson(accountId) {

    logMessage('Account ' + accountId + 'added to page');
    
    window.lpTag = window.lpTag || {};
    if (typeof window.lpTag._tagCount === 'undefined') {
        window.lpTag = {
        site: accountId,
        section: lpTag.section || '',
        autoStart: lpTag.autoStart === false ? false : true,
        ovr: lpTag.ovr || {},
        _v: '1.6.0',
        _tagCount: 1,
        protocol: 'https:',
        events: {
        bind: function (app, ev, fn) {
            lpTag.defer(function () {
                        lpTag.events.bind(app, ev, fn);
                        }, 0);
        }, trigger: function (app, ev, json) {
            lpTag.defer(function () {
                        lpTag.events.trigger(app, ev, json);
                        }, 1);
        }
        },
        defer: function (fn, fnType) {
            if (fnType == 0) {
                this._defB = this._defB || [];
                this._defB.push(fn);
            } else if (fnType == 1) {
                this._defT = this._defT || [];
                this._defT.push(fn);
            } else {
                this._defL = this._defL || [];
                this._defL.push(fn);
            }
        },
        load: function (src, chr, id) {
            var t = this;
            setTimeout(function () {
                       t._load(src, chr, id);
                       }, 0);
        },
        _load: function (src, chr, id) {
            var url = src;
            if (!src) {
                url = this.protocol + '//' + ((this.ovr && this.ovr.domain) ? this.ovr.domain : 'lptag.liveperson.net') + '/tag/tag.js?site=' + this.site;
            }
            var s = document.createElement('script');
            s.setAttribute('charset', chr ? chr : 'UTF-8');
            if (id) {
                s.setAttribute('id', id);
            }
            s.setAttribute('src', url);
            document.getElementsByTagName('head').item(0).appendChild(s);
        },
        init: function () {
            this._timing = this._timing || {};
            this._timing.start = (new Date()).getTime();
            var that = this;
            if (window.attachEvent) {
                window.attachEvent('onload', function () {
                                   that._domReady('domReady');
                                   });
            } else {
                window.addEventListener('DOMContentLoaded', function () {
                                        that._domReady('contReady');
                                        }, false);
                window.addEventListener('load', function () {
                                        that._domReady('domReady');
                                        }, false);
            }
            if (typeof (window._lptStop) == 'undefined') {
                this.load();
            }
        },
        start: function () {
            this.autoStart = true;
        },
        _domReady: function (n) {
            if (!this.isDom) {
                this.isDom = true;
                this.events.trigger('LPT', 'DOM_READY', {t: n});
            }
            this._timing[n] = (new Date()).getTime();
        },
        vars: lpTag.vars || [],
        dbs: lpTag.dbs || [],
        ctn: lpTag.ctn || [],
        sdes: lpTag.sdes || [],
        ev: lpTag.ev || []
        };
        lpTag.init();
    } else {
        window.lpTag._tagCount += 1;
    }
    
    lpTag.events.bind({
                      eventName: 'cobrowseAccepted',
                      appName: '*',
                      func: function (event) {
                      var elements = document.querySelectorAll('[data-assist-a' + event.agentId + ']');
                      var data = JSON.parse(atob(elements[elements.length - 1].dataset['assistA' + event.agentId]));
                      //                      window.webkit.messageHandlers.assistView.postMessage(JSON.stringify(data));
                      sendMessage(JSON.stringify(data), "coBrowse");
                      },
                      async: true
                      });
    
    
    lpTag.events.bind({
                      eventName: 'maximized',
                      appName: '*',
                      func: function (event) {
                      
                      window.setTimeout(function () {
                                        getLivePersonRectangles();
                                        }, elementToAppearWaitTimeInMS);
                      },
                      async: true
                      });
    
    lpTag.events.bind({
                      eventName: 'minimized',
                      appName: '*',
                      func: function (event) {
                      
                      window.setTimeout(function () {
                                        getLivePersonRectangles();
                                        }, elementToAppearWaitTimeInMS);
                      
                      },
                      async: true
                      });
    
    
    lpTag.events.bind(
                      {
                      appName: '*',
                      eventName: 'state',
                      func: function (e) {
                      sendMessage(e, "state");
                      }
                      });
    
    lpTag.events.bind({
                      eventName: '*',
                      appName: '*',
                      func: function (event) {
                      
                      console.log(event);
                      window.setTimeout(function () {
                                        getLivePersonRectangles();
                                        }, elementToAppearWaitTimeInMS);
                      
                      
                      if (event.state == 'ended') {
                      sendMessage("Chat has ended..", "endSupport");
                      }
                      
                      
                      if (event.instantiated) {
                      
                      window.setTimeout(function () {
                                        getLivePersonRectangles();
                                        }, elementToAppearWaitTimeInMS);
                      
                      }
                      
                      if (eventName.indexOf('init') !== -1) {
                      
                      window.setTimeout(function () {
                                        getLivePersonRectangles();
                                        }, elementToAppearWaitTimeInMS);
                      
                      addCloseListener();
                      
                      }
                      
                      
                      },
                      async: true
                      });
    
    
}

function addCloseListener() {
    
    var close = document.getElementsByClassName("lp_close");
    for (var i = 0; i < close.length; i++) {
        close[i].addEventListener('click', function () {
                                  window.setTimeout(function () {
                                                    getLivePersonRectangles();
                                                    }, elementToAppearWaitTimeInMS);
                                  });
    }
}

function logMessage(message) {
    sendMessage(message, "log");
}

function sendMessage(message, type) {
    var jsonMessage = {type: type, message: message};
    console.log(message);
    window.location.href = "assist://" + JSON.stringify(jsonMessage);
}

function getLivePersonRectangles() {
    var maximized = document.getElementsByClassName("lp_maximized")[0];
    var minimized = document.getElementsByClassName("lp_minimized")[0];
    
    var viewData = {
    innerWidth: window.innerWidth,
    innerHeight: window.innerHeight,
    rectangles: []
    };
    
    var livePersonButtons = document.getElementsByClassName("LPMcontainer");
    
    for (var i = 0; i != livePersonButtons.length; i++) {
        addElementRectangleToRectangleList2("LPMcontainer", viewData.rectangles, i);
    }
    
    addElementRectangleToRectangleList("lp_maximized", viewData.rectangles);
    addElementRectangleToRectangleList("lp_minimized", viewData.rectangles);
    
    var rectangles = JSON.stringify(viewData);
    sendMessage(rectangles, "rectangles");
}

function addElementRectangleToRectangleList(elementName, rectangleList) {
    var element = document.getElementsByClassName(elementName)[0];
    
    if (element) {
        var rect = element.getBoundingClientRect();
        rectangleList.push({
                           name: elementName,
                           left: rect.left,
                           right: rect.right,
                           top: rect.top,
                           bottom: rect.bottom,
                           width: rect.width,
                           height: rect.height
                           });
    }
}

function addElementRectangleToRectangleList2(elementName, rectangleList, index) {
    var element = document.getElementsByClassName(elementName)[index];
    
    if (element) {
        var rect = element.getBoundingClientRect();
        rectangleList.push({
                           name: elementName,
                           left: rect.left,
                           right: rect.right,
                           top: rect.top,
                           bottom: rect.bottom,
                           width: rect.width,
                           height: rect.height
                           });
    }
}

function setSection(section) {
    lpTag.section = section.split(',');
    logMessage(section);
    
}

function forceRectangleUpdate() {
    getLivePersonRectangles();
}
